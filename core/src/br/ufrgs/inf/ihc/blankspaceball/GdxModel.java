package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.graphics.g3d.ModelInstance;

public interface GdxModel {
    ModelInstance getModelInstance();
}
