package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class MenuControllerOptions implements Screen, InputProcessor {

    private Logger logger;
    public GameEntry game;
    public MenuMain menuMain;
    public MenuController menuController;
    private OrthographicCamera camera;

    public MenuControllerOptions(Logger logger, GameEntry game, MenuMain menuMain, MenuController menuController) {

        this.logger = logger;
        this.game = game;
        this.menuMain = menuMain;
        this.menuController = menuController;

        camera = new OrthographicCamera();
        logger.debug("menuMain camera", camera.toString());
        camera.setToOrtho(false, 800, 480);
        game.batch.setProjectionMatrix(camera.combined);

        Gdx.input.setInputProcessor(this);

    }


    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.BACK) {
            game.setScreen(menuController);
            Gdx.input.setInputProcessor(menuController);
        }

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        game.batch.begin();

        game.font.getData().setScale(4f);
        game.font.setColor(Color.WHITE);
        game.font.draw(game.batch, "Controller Options ", 190, 435);
        game.font.getData().setScale(2f);

        if (game.controllerType == ControllerType.BLUETOOTH) {
            game.speedButtonPlus.setPosition(460, 245);
            game.speedButtonPlus.setSize(80, 80);
            game.speedButtonPlus.setAlpha(1f);

            game.speedButtonMinus.setPosition(290, 245);
            game.speedButtonMinus.setSize(80, 80);
            game.speedButtonMinus.setAlpha(1f);

            game.horizontalFlipButton.setPosition(270, 155);
            game.horizontalFlipButton.setSize(300, 50);
            game.horizontalFlipButton.setAlpha(1f);

            game.verticalFlipButton.setPosition(270, 65);
            game.verticalFlipButton.setSize(300, 50);
            game.verticalFlipButton.setAlpha(1f);

            if (Gdx.input.justTouched()) {
                Vector3 touchCoordinates = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
                Vector3 screenCoordinates = camera.unproject(touchCoordinates);

                if (game.speedButtonPlus.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    float speed = game.preferences.getFloat("speedBluetooth");
                    game.preferences.putFloat("speedBluetooth",  speed + 1);
                    game.speedButtonPlus.setAlpha(0.5f);
                }
                else if (game.speedButtonMinus.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    float speed = game.preferences.getFloat("speedBluetooth");
                    if (speed >= 1)
                        game.preferences.putFloat("speedBluetooth",  speed - 1);
                    game.speedButtonMinus.setAlpha(0.5f);
                }
                else if (game.verticalFlipButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    boolean flip = game.preferences.getBoolean("invertYBluetooth");
                    game.preferences.putBoolean("invertYBluetooth",  (!flip));
                    game.verticalFlipButton.setAlpha(0.5f);
                }
                else if (game.horizontalFlipButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    boolean flip = game.preferences.getBoolean("invertXBluetooth");
                    game.preferences.putBoolean("invertXBluetooth",  (!flip));
                    game.horizontalFlipButton.setAlpha(0.5f);
                }
            }

            game.speedButtonPlus.draw(game.batch);
            game.speedButtonMinus.draw(game.batch);
            game.horizontalFlipButton.draw(game.batch);
            game.verticalFlipButton.draw(game.batch);

            game.font.draw(game.batch, "Speed", 355, 355);
            game.font.draw(game.batch, "-", 325, 300);
            game.font.draw(game.batch, "+", 495, 300);
            game.font.draw(game.batch, String.valueOf((int) game.preferences.getFloat("speedBluetooth")), 405, 300);

            String flipHorizontal = "Flip Horizontal";
            if (game.preferences.getBoolean("invertXBluetooth"))
                flipHorizontal += " [ON]";
            else
                flipHorizontal += " [OFF]";
            game.font.draw(game.batch, flipHorizontal, 300, 195);

            String flipVertical = "Flip Vertical";
            if (game.preferences.getBoolean("invertYBluetooth"))
                flipVertical += " [ON]";
            else
                flipVertical += " [OFF]";
            game.font.draw(game.batch, flipVertical, 315, 105);
        }
        else if (game.controllerType == ControllerType.MOTION) {
            // First column
            game.sensitivityPlus.setPosition(260, 265);
            game.sensitivityPlus.setSize(80, 80);
            game.sensitivityPlus.setAlpha(1f);

            game.sensitivityMinus.setPosition(90, 265);
            game.sensitivityMinus.setSize(80, 80);
            game.sensitivityMinus.setAlpha(1f);

            game.amplitudeXButtonPlus.setPosition(260, 145);
            game.amplitudeXButtonPlus.setSize(80, 80);
            game.amplitudeXButtonPlus.setAlpha(1f);

            game.amplitudeXButtonMinus.setPosition(90, 145);
            game.amplitudeXButtonMinus.setSize(80, 80);
            game.amplitudeXButtonMinus.setAlpha(1f);

            game.amplitudeYButtonPlus.setPosition(260, 25);
            game.amplitudeYButtonPlus.setSize(80, 80);
            game.amplitudeYButtonPlus.setAlpha(1f);

            game.amplitudeYButtonMinus.setPosition(90, 25);
            game.amplitudeYButtonMinus.setSize(80, 80);
            game.amplitudeYButtonMinus.setAlpha(1f);

            // Second column
            game.horizontalFlipButton.setPosition(450, 225);
            game.horizontalFlipButton.setSize(300, 50);
            game.horizontalFlipButton.setAlpha(1f);

            game.verticalFlipButton.setPosition(450, 110);
            game.verticalFlipButton.setSize(300, 50);
            game.verticalFlipButton.setAlpha(1f);

            if (Gdx.input.justTouched()) {
                Vector3 touchCoordinates = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
                Vector3 screenCoordinates = camera.unproject(touchCoordinates);

                if (game.sensitivityPlus.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    float sensitivity = game.preferences.getFloat("sensitivityMotion");
                    if (sensitivity + 10 <= 100)
                        game.preferences.putFloat("sensitivityMotion",  sensitivity + 10);
                    game.sensitivityPlus.setAlpha(0.5f);
                }
                else if (game.sensitivityMinus.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    float sensitivity = game.preferences.getFloat("sensitivityMotion");
                    if (sensitivity - 10 >= 10)
                        game.preferences.putFloat("sensitivityMotion",  sensitivity - 10);
                    game.sensitivityMinus.setAlpha(0.5f);
                }
                else if (game.amplitudeXButtonPlus.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    float amplitude = game.preferences.getFloat("amplitudeXMotion");
                    if (amplitude + 5 <= 90)
                        game.preferences.putFloat("amplitudeXMotion",  amplitude + 5);
                    game.amplitudeXButtonPlus.setAlpha(0.5f);
                }
                else if (game.amplitudeXButtonMinus.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    float amplitude = game.preferences.getFloat("amplitudeXMotion");
                    if (amplitude - 5 >= 30)
                        game.preferences.putFloat("amplitudeXMotion",  amplitude - 5);
                    game.amplitudeXButtonMinus.setAlpha(0.5f);
                }
                else if (game.amplitudeYButtonPlus.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    float amplitude = game.preferences.getFloat("amplitudeYMotion");
                    if (amplitude + 5 <= 90)
                        game.preferences.putFloat("amplitudeYMotion",  amplitude + 5);
                    game.amplitudeYButtonPlus.setAlpha(0.5f);
                }
                else if (game.amplitudeYButtonMinus.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    float amplitude = game.preferences.getFloat("amplitudeYMotion");
                    if (amplitude - 5 >= 30)
                        game.preferences.putFloat("amplitudeYMotion",  amplitude - 5);
                    game.amplitudeYButtonMinus.setAlpha(0.5f);
                }
                else if (game.verticalFlipButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    boolean flip = game.preferences.getBoolean("invertYMotion");
                    game.preferences.putBoolean("invertYMotion",  !flip);
                    game.verticalFlipButton.setAlpha(0.5f);
                }
                else if (game.horizontalFlipButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                    boolean flip = game.preferences.getBoolean("invertXMotion");
                    game.preferences.putBoolean("invertXMotion",  !flip);
                    game.horizontalFlipButton.setAlpha(0.5f);
                }
            }

            game.sensitivityPlus.draw(game.batch);
            game.sensitivityMinus.draw(game.batch);
            game.amplitudeXButtonPlus.draw(game.batch);
            game.amplitudeXButtonMinus.draw(game.batch);
            game.amplitudeYButtonPlus.draw(game.batch);
            game.amplitudeYButtonMinus.draw(game.batch);
            game.horizontalFlipButton.draw(game.batch);
            game.verticalFlipButton.draw(game.batch);

            game.font.draw(game.batch, "Sensitivity", 155, 375);
            game.font.draw(game.batch, "-", 125, 320);
            game.font.draw(game.batch, "+", 295, 320);
            game.font.draw(game.batch, String.valueOf((int)game.preferences.getFloat("sensitivityMotion")), 195, 320);
            game.font.draw(game.batch, "Horizontal Amplitude", 105, 255);
            game.font.draw(game.batch, "-", 125, 205);
            game.font.draw(game.batch, "+", 295, 205);
            game.font.draw(game.batch, String.valueOf((int)game.preferences.getFloat("amplitudeXMotion")), 195, 205);
            game.font.draw(game.batch, "Vertical Amplitude", 105, 140);
            game.font.draw(game.batch, "-", 125, 75);
            game.font.draw(game.batch, "+", 295, 75);
            game.font.draw(game.batch, String.valueOf((int)game.preferences.getFloat("amplitudeYMotion")), 195, 75);

            String flipHorizontal = "Flip Horizontal";
            if (game.preferences.getBoolean("invertXMotion"))
                flipHorizontal += " [ON]";
            else
                flipHorizontal += " [OFF]";
            game.font.draw(game.batch, flipHorizontal, 480, 265);

            String flipVertical = "Flip Vertical";
            if (game.preferences.getBoolean("invertYMotion"))
                flipVertical += " [ON]";
            else
                flipVertical += " [OFF]";
            game.font.draw(game.batch, flipVertical, 495, 150);
        }
        game.batch.end();
        game.preferences.flush();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
