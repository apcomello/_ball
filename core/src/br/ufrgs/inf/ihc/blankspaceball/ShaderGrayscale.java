package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.shaders.BaseShader;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

class ShaderGrayscale extends BaseShader {
    protected final ShaderProgram program;

    public ShaderGrayscale(Renderable renderable) {
        super();
        FileHandle vertexShader = Gdx.files.internal("vertex_shader.glsl");
        FileHandle fragmentShader = Gdx.files.internal("fragment_shader.glsl");
        program = new ShaderProgram(vertexShader, fragmentShader);
    }

    @Override
    public void init () {
        super.init(program, null);
    }

    @Override
    public int compareTo (Shader other) {
        return 0;
    }

    @Override
    public boolean canRender (Renderable instance) {
        return true;
    }

    @Override
    public void begin (Camera camera, RenderContext context) {
        this.camera = camera;
        this.context = context;
        program.begin();
        program.setUniformMatrix("u_projViewTrans", camera.combined);
    }

    @Override
    public void render (Renderable renderable) {
        program.setUniformMatrix("u_worldTrans", renderable.worldTransform);

        // Color
        ColorAttribute colorAttr = (ColorAttribute) renderable.material.get(ColorAttribute.Diffuse);
        program.setUniformf("u_color", colorAttr.color);

        // Texture
        TextureAttribute textureAttrib = (TextureAttribute) renderable.material.get(TextureAttribute.Diffuse);

        Texture texture = null;
        if (textureAttrib != null)
            texture = textureAttrib.textureDescription.texture;

        context.setBlending(true, GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        context.setDepthMask(true);
        context.setDepthTest(GL20.GL_LEQUAL);
        context.setCullFace(GL20.GL_BACK);

        if (texture != null) {
            program.setUniformi("u_has_texture", 1);
            program.setUniformi("u_texture", this.context.textureBinder.bind(texture));
        }
        else
        {
            program.setUniformi("u_has_texture", 0);
        }

        // Alpha
        BlendingAttribute blendAttr = (BlendingAttribute) renderable.material.get(BlendingAttribute.Type);
        if (blendAttr != null) {
            program.setUniformf("u_alpha", blendAttr.opacity);
        }
        else
            program.setUniformf("u_alpha", 1f);

        renderable.meshPart.render(program);
    }

    @Override
    public void end () {
        program.end();
    }

    @Override
    public void dispose () {
        super.dispose();
        program.dispose();
    }
}