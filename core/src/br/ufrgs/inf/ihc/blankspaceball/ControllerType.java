package br.ufrgs.inf.ihc.blankspaceball;

public enum ControllerType {
    TOUCH,
    PICTURE_IN_PICTURE,
    MOTION,
    BLUETOOTH;

    static GameController getController(ControllerType controllerType, Object ... objects) {
        if (objects.length == 0)
            throw new IllegalArgumentException("Must inform at least one object for controller initialization.");

        switch(controllerType) {
            case TOUCH:
                return new ControllerTouchScreen((GameScreen) objects[0], (Logger) objects[1], (MenuMain) objects[2], (GameEntry) objects[3]);
            case PICTURE_IN_PICTURE:
                return new ControllerSplitScreen((GameScreen) objects[0], (Logger) objects[1], (MenuMain) objects[2], (GameEntry) objects[3]);
            case MOTION:
                return new ControllerMotion((GameScreen) objects[0], (Logger) objects[1], (MenuMain) objects[2], (GameEntry) objects[3]);
            case BLUETOOTH:
                return new ControllerBluetooth((GameScreen) objects[0], (Logger) objects[1], (MenuMain) objects[2], (GameEntry) objects[3]);
            default:
                return null;
        }
    }
}
