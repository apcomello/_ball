package br.ufrgs.inf.ihc.blankspaceball;

public interface GameScreen {
    void pointerHover(int screenX, int screenY);
    void pointerDown(int screenX, int screenY);
    void pointerDrag(int screenX, int screenY);
    void pointerUp(int screenX, int screenY);
    void analog(float axisX, float axisY);
    void motion(float axisX, float axisY);
    void buttonPress(int buttonCode);
}
