package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class MenuMain implements Screen {

    final private GameEntry game;

    private OrthographicCamera camera;

    private Logger logger;

    public MenuMain(Logger logger, GameEntry game) {
        this.game = game;
        this.logger = logger;

        camera = new OrthographicCamera();
        logger.debug("menuMain camera", camera.toString());
        camera.setToOrtho(false, 800, 480);
        game.batch.setProjectionMatrix(camera.combined);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        game.batch.begin();

        game.startButton.setPosition(300, 210);
        game.startButton.setSize(200, 50);
        game.startButton.setAlpha(1f);

        game.optionsButton.setPosition(300, 110);
        game.optionsButton.setSize(200, 50);
        game.optionsButton.setAlpha(1f);

        if (Gdx.input.justTouched()) {
            Vector3 touchCoordinates = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            Vector3 screenCoordinates = camera.unproject(touchCoordinates);

            if (game.startButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                game.startButton.setAlpha(0.5f);
                game.setScreen(new GameMain(logger, game, this, true)); // Starts game
                dispose();
            }
            else if (game.optionsButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                game.optionsButton.setAlpha(0.5f);
                game.setScreen(new MenuOptions(logger, game, this)); // Controller menuMain
                dispose();
            }
        }
        game.startButton.draw(game.batch);
        game.optionsButton.draw(game.batch);

        game.font.getData().setScale(4f);
        game.font.setColor(Color.WHITE);
        game.font.draw(game.batch, "BlankSpaceBall ", 200, 350);
        game.font.getData().setScale(2f);
        game.font.draw(game.batch, "Tap to play", 330, 250);
        game.font.draw(game.batch, "Options", 350, 150);
        game.batch.end();

    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
    }
}
