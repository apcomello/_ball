package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class MenuOptions implements Screen, InputProcessor {

    private Logger logger;
    public GameEntry game;
    public MenuMain menuMain;
    private OrthographicCamera camera;

    public MenuOptions(Logger logger, GameEntry game, MenuMain menuMain) {

        this.logger = logger;
        this.game = game;
        this.menuMain = menuMain;

        camera = new OrthographicCamera();
        logger.debug("options menuMain camera", camera.toString());
        camera.setToOrtho(false, 800, 480);
        game.batch.setProjectionMatrix(camera.combined);

        Gdx.input.setInputProcessor(this);

    }


    @Override
    public void show() {
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        game.batch.begin();

        game.soundsButton.setPosition(300, 210);
        game.soundsButton.setSize(200, 50);
        game.soundsButton.setAlpha(1f);

        game.controllersButton.setPosition(300, 110);
        game.controllersButton.setSize(200, 50);
        game.controllersButton.setAlpha(1f);

        if (Gdx.input.justTouched()) {
            Vector3 touchCoordinates = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            Vector3 screenCoordinates = camera.unproject(touchCoordinates);

            if (game.controllersButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                game.controllersButton.setAlpha(0.5f);
                game.setScreen(new MenuController(logger, game, menuMain, this)); // Controller menuMain
                dispose();
            }
        }

        game.soundsButton.draw(game.batch);
        game.controllersButton.draw(game.batch);

        game.font.getData().setScale(4f);
        game.font.setColor(Color.WHITE);
        game.font.draw(game.batch, "Options ", 295, 350);
        game.font.getData().setScale(2f);
        game.font.draw(game.batch, "Controllers", 330, 150);
        game.font.setColor(Color.LIGHT_GRAY);
        game.font.draw(game.batch, "Sounds", 350, 250);
        game.font.setColor(Color.WHITE);
        game.batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.BACK)
            game.setScreen(menuMain);

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
