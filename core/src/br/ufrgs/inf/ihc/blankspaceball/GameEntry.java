package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameEntry extends Game {

    public SpriteBatch batch;
    public BitmapFont font;

    // Start screen
    public Sprite startButton;
    public Sprite optionsButton;

    // MenuOptions screen
    public Sprite soundsButton;
    public Sprite controllersButton;

    // Controllers screen
    public Sprite controllerTypeButton;
    public Sprite controllerOptionsButton;

    // Choose controllers screen
    public Sprite touchControllerButton;
    public Sprite pipControllerButton;
    public Sprite bluetoothControllerButton;
    public Sprite motionControllerButton;

    // Controller options screen - Bluetooth
    public Sprite speedButtonPlus;
    public Sprite speedButtonMinus;
    public Sprite amplitudeXButtonPlus;
    public Sprite amplitudeXButtonMinus;
    public Sprite amplitudeYButtonPlus;
    public Sprite amplitudeYButtonMinus;
    public Sprite sensitivityPlus;
    public Sprite sensitivityMinus;
    public Sprite horizontalFlipButton;
    public Sprite verticalFlipButton;

    ControllerType controllerType;

    private Texture buttonTexture;
    private Logger logger;

    public Preferences preferences;

    GameEntry(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();

        buttonTexture = new Texture(Gdx.files.internal("button.png"));
        startButton = new Sprite(buttonTexture);
        optionsButton = new Sprite(buttonTexture);
        touchControllerButton = new Sprite(buttonTexture);
        pipControllerButton = new Sprite(buttonTexture);
        bluetoothControllerButton = new Sprite(buttonTexture);
        motionControllerButton = new Sprite(buttonTexture);
        soundsButton = new Sprite(buttonTexture);
        controllersButton = new Sprite(buttonTexture);
        controllerTypeButton = new Sprite(buttonTexture);
        controllerOptionsButton = new Sprite(buttonTexture);
        speedButtonPlus = new Sprite(buttonTexture);
        speedButtonMinus = new Sprite(buttonTexture);
        amplitudeXButtonPlus = new Sprite(buttonTexture);
        amplitudeXButtonMinus = new Sprite(buttonTexture);
        amplitudeYButtonPlus = new Sprite(buttonTexture);
        amplitudeYButtonMinus = new Sprite(buttonTexture);
        sensitivityPlus = new Sprite(buttonTexture);
        sensitivityMinus = new Sprite(buttonTexture);
        horizontalFlipButton = new Sprite(buttonTexture);
        verticalFlipButton = new Sprite(buttonTexture);
        preferences = Gdx.app.getPreferences("GAME_INFO");

        if (preferences.contains("controllerType"))
            controllerType = ControllerType.values()[preferences.getInteger("controllerType")];
        else
            controllerType = ControllerType.TOUCH;

        // Set default preferences

        // Bluetooth
        if (!preferences.contains("invertXBluetooth"))
            preferences.putBoolean("invertXBluetooth", false);
        if (!preferences.contains("invertYBluetooth"))
            preferences.putBoolean("invertYBluetooth", false);
        if (!preferences.contains("speedBluetooth"))
            preferences.putFloat("speedBluetooth", 2f);

        // Motion
        if (!preferences.contains("invertXMotion"))
            preferences.putBoolean("invertXMotion", false);
        if (!preferences.contains("invertYMotion"))
            preferences.putBoolean("invertYMotion", false);
        if (!preferences.contains("sensitivityMotion"))
            preferences.putFloat("sensitivityMotion", 40f);
        if (!preferences.contains("amplitudeXMotion"))
            preferences.putFloat("amplitudeXMotion", 60f);
        if (!preferences.contains("amplitudeYMotion"))
            preferences.putFloat("amplitudeYMotion", 60f);

        preferences.flush();

        this.setScreen(new MenuMain(logger, this));
    }

    public void render() {
        super.render(); // important
    }

    public void dispose() {

    }
}
