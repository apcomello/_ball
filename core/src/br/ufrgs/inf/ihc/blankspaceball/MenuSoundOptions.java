package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class MenuSoundOptions implements Screen, InputProcessor {

    private Logger logger;
    public GameEntry game;
    public MenuOptions menuOptions;
    private OrthographicCamera camera;

    public MenuSoundOptions(Logger logger, GameEntry game, MenuOptions menuOptions) {

        this.logger = logger;
        this.game = game;
        this.menuOptions = menuOptions;

        camera = new OrthographicCamera();
        logger.debug("sound options menuMain camera", camera.toString());
        camera.setToOrtho(false, 800, 480);
        game.batch.setProjectionMatrix(camera.combined);

        Gdx.input.setInputProcessor(this);

    }


    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.BACK) {
            game.setScreen(menuOptions);
            Gdx.input.setInputProcessor(menuOptions);
        }

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void show() {
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        game.batch.begin();

        game.controllersButton.setPosition(310, 110);
        game.controllersButton.setSize(170, 50);
        game.controllersButton.draw(game.batch);

        game.soundsButton.setPosition(310, 210);
        game.soundsButton.setSize(200, 50);
        game.soundsButton.draw(game.batch);

        game.font.getData().setScale(4f);
        game.font.setColor(Color.WHITE);
        game.font.draw(game.batch, "Sound ", 250, 350);
        game.font.getData().setScale(2f);
        game.font.draw(game.batch, "Controllers", 350, 150);
        game.font.draw(game.batch, "Sounds", 350, 250);
        game.batch.end();

        // TODO: figure out how to make it happen on touchUp instead of touchDown
        // TODO: make mock up for buttons
//        if (Gdx.input.isTouched()) {
//            Vector3 touchCoordinates = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
//            Vector3 screenCoordinates = camera.unproject(touchCoordinates);
//        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
