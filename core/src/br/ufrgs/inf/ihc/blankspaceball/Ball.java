package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.utils.Disposable;

public class Ball extends Entity implements GdxModel, BtCollision, Disposable {
    // basic members
	private Vector3 position = new Vector3(0f, 0f, 0f);
	private Vector3 velocity = new Vector3(0f, 0f, 0f);
	private float radius = 1f;
	private Color color = Color.BLACK;
	private float alphaBlend = 1f;

	// model members
    private Model model;
    private ModelBuilder modelBuilder;
    private ModelInstance modelInstance;

    // collision members
    private btCollisionShape collisionShape;
    private btCollisionObject collisionObject;

    // logging
    private Logger logger;

    Ball(Logger logger) {
        this.logger = logger;
    }

	// region Getters
	public Vector3 getPosition() {
		return this.position.cpy();
	}

	public Vector3 getVelocity() {
		return this.velocity.cpy();
	}

	public float getRadius() {
		return this.radius;
	}

    public Color getColor() {
        return this.color.cpy();
    }
    // endregion

    // region Setters
    public Ball setPosition(Vector3 position) {
        this.position = position.cpy();

        if (this.modelInstance != null)
            recreateModelInstance();

        return this;
    }

    public Ball setPosition(float x, float y, float z) {
        return this.setPosition(new Vector3(x, y, z));
    }

    public Ball setVelocity(Vector3 velocity) {
        this.velocity = velocity.cpy();
        return this;
    }

    public Ball setVelocity(float x, float y, float z) {
        return this.setVelocity(new Vector3(x, y, z));
    }

    public Ball setRadius(float radius) {
        this.radius = radius;

        if (this.modelInstance != null)
            recreateModelInstance();

        return this;
    }

    public Ball setColor(Color color) {
        this.color = color.cpy();

        if (this.color.equals(Color.CLEAR))
            this.alphaBlend = 0f;

        if (this.modelInstance != null)
            recreateModelInstance();

        return this;
    }
    //endregion

    public ModelInstance getModelInstance() {
	    if (modelInstance != null)
	        return modelInstance;

	    if (modelBuilder == null)
	        modelBuilder = new ModelBuilder();

        if (color.equals(Color.CLEAR))
            alphaBlend = 0f;

        model = modelBuilder.createSphere(
                this.radius * 2, this.radius * 2, this.radius * 2,
                72, 72,
                new Material(
                        ColorAttribute.createDiffuse(this.color),
                        new BlendingAttribute(this.alphaBlend)
                ),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal
        );
        modelInstance = new ModelInstance(model);
        modelInstance.transform.translate(this.position);

        return modelInstance;
    }

    public void recreateModelInstance() {
        this.modelInstance = null;
        getModelInstance();

        if (collisionObject != null)
            recreateCollisionObject();
    }

    public btCollisionObject getCollisionObject() {
	    if (collisionObject != null)
            return collisionObject;

	    if (modelInstance == null)
	        modelInstance = getModelInstance();

        collisionShape = new btSphereShape(this.radius);
        collisionObject = new btCollisionObject();
        collisionObject.setCollisionShape(collisionShape);
        collisionObject.setWorldTransform(modelInstance.transform);

        return collisionObject;
    }

    public void recreateCollisionObject() {
        collisionObject = null;
        getCollisionObject();
    }

    public void displace(Vector3 displacement) {
        this.position.add(displacement);

        if (modelInstance != null) {
            modelInstance.transform.translate(displacement);

            if (collisionObject != null)
                collisionObject.setWorldTransform(modelInstance.transform);
        }
    }

    public void displaceByFactorAmount(float factor) {
        Vector3 displacement = this.velocity.cpy();
        displacement.scl(factor);
        displace(displacement);
    }

    @Override
    public void dispose() {
	    model.dispose();
	    collisionShape.dispose();
	    collisionObject.dispose();
    }
}