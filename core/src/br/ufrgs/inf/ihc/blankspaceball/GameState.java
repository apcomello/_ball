package br.ufrgs.inf.ihc.blankspaceball;

public enum GameState {
    INITIAL_STATE,
    PLAYING_STATE,
    PAUSED_STATE,
    ENDED_WIN_STATE,
    ENDED_LOSS_STATE;
}
