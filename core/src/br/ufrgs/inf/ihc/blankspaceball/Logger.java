package br.ufrgs.inf.ihc.blankspaceball;

public interface Logger {
    void debug(String tag, String message);
    void error(String tag, String message);
    void info(String tag, String message);
    void warn(String tag, String message);
}
