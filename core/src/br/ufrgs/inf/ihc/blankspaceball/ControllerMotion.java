package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector3;

public class ControllerMotion extends GameController implements InputProcessor {
    private Boolean listening;
    private Boolean touched;
    private Boolean invertX;
    private Boolean invertY;
    private float sensitivity;
    private float amplitudeDegX;
    private float amplitudeDegY;
    private float lastValidX;
    private float lastValidY;

    ControllerMotion(GameScreen screen, Logger logger, MenuMain menuMain, GameEntry game) {
        super(screen, logger, menuMain, game);

        listening = false;
        touched = false;
        invertX = game.preferences.getBoolean("invertXMotion");
        invertY = game.preferences.getBoolean("invertYMotion");
        sensitivity = game.preferences.getFloat("sensitivityMotion");
        amplitudeDegX = game.preferences.getFloat("amplitudeXMotion");
        amplitudeDegY = game.preferences.getFloat("amplitudeYMotion");
        lastValidX = Float.MAX_VALUE;
        lastValidY = Float.MAX_VALUE;

        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void movePaddle(float axisX, float axisY, Paddle p1Paddle) {
        Vector3 paddleCenter = p1Paddle.getCenter();

        // calculate new position by moving only along x and y
        float newPosX = axisX * (4f - 0.75f);
        float newPosY = axisY * -1 * (2.5f - 0.5f);
        Vector3 newPosition = new Vector3(newPosX, newPosY, paddleCenter.z);

        // move paddle
        Vector3 displacement = newPosition.cpy().sub(paddleCenter);
        p1Paddle.displace(displacement);
    }

    @Override
    public void poll() {
        // start listening after the input is polled the first time
        if (!listening)
            listening = true;

        // read both rotation sensors in the -180 ~ 180 range
        float axisX = Gdx.input.getPitch();
        float axisY = Gdx.input.getRoll();

        // clamp them to their amplitudes
        axisX = Math.min(Math.max(axisX, -amplitudeDegX / 2f), amplitudeDegX / 2f);
        axisY = Math.min(Math.max(axisY, -amplitudeDegY / 2f), amplitudeDegY / 2f);

        // convert to -1 ~ 1 range
        axisX /= amplitudeDegX / 2f;
        axisY /= amplitudeDegY / 2f;

        // invert X and Y
        if (invertX)
            axisX *= -1f;
        if (invertY)
            axisY *= -1f;

        // noise reduction
        if (lastValidX != Float.MAX_VALUE && lastValidY != Float.MAX_VALUE) {
            // IIR
            float alpha = sensitivity / 100f;
            axisX = alpha * axisX + (1f - alpha) * lastValidX;
            axisY = alpha * axisY + (1f - alpha) * lastValidY;
        }
        lastValidX = axisX;
        lastValidY = axisY;

        // move the paddle with the calculated axis values
        screen.motion(axisX, axisY);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.BACK)
            game.setScreen(menuMain);

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!listening || touched)
            return false;

        touched = true;

        screen.pointerDown(screenX, screenY);

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (!listening || !touched)
            return false;

        screen.pointerUp(screenX, screenY);

        touched = false;

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (!listening || !touched)
            return false;

        screen.pointerDrag(screenX, screenY);

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (!listening || !touched)
            return false;

        screen.pointerDrag(screenX, screenY);

        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
