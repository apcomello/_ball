package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;

public class ControllerBluetooth extends GameController implements ControllerListener, InputProcessor {
    private float x;
    private float y;
    private float movementScale;
    private Boolean invertX;
    private Boolean invertY;

    ControllerBluetooth(GameScreen screen, Logger logger, MenuMain menuMain, GameEntry game) {
        super(screen, logger, menuMain, game);
        x = 0;
        y = 0;
        movementScale = game.preferences.getFloat("speedBluetooth");
        invertX = game.preferences.getBoolean("invertXBluetooth");
        invertY = game.preferences.getBoolean("invertYBluetooth");

        Controllers.addListener(this);
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void movePaddle(float axisX, float axisY, Paddle p1Paddle) {
        Vector3 paddleCenter = p1Paddle.getCenter();

        // calculate new position by moving only along x and y
        float newPosX = paddleCenter.x + axisX * -1f * (4f - 0.75f) / 50f * movementScale;
        float newPosY = paddleCenter.y + axisY * -1f * (2.5f - 0.5f) / 50f * movementScale;

        // clamp to screen bounds
        newPosX = Math.min(Math.max(newPosX, -4f + 0.75f), 4f - 0.75f);
        newPosY = Math.min(Math.max(newPosY, -2.5f + 0.5f), 2.5f - 0.5f);

        // move paddle
        Vector3 newPosition = new Vector3(newPosX, newPosY, paddleCenter.z);
        Vector3 displacement = newPosition.cpy().sub(paddleCenter);
        p1Paddle.displace(displacement);
    }

    @Override
    public void poll() {
        screen.analog(x, y);
    }

    @Override
    public void connected(Controller controller) {
    }

    @Override
    public void disconnected(Controller controller) {
    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        screen.buttonPress(buttonCode);

        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        // update axes
        if (axisCode == 0) {
            y = value;

            // invert Y
            if (invertY)
                y *= -1f;
        }
        else if (axisCode == 1) {
            x = value;

            // invert X
            if (invertX)
                x *= -1f;
        }

        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        return false;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.BACK)
            game.setScreen(menuMain);

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
