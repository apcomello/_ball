package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class MenuController implements Screen, InputProcessor {

    private Logger logger;
    public GameEntry game;
    public MenuMain menuMain;
    public MenuOptions options;
    private OrthographicCamera camera;

    public MenuController(Logger logger, GameEntry game, MenuMain menuMain, MenuOptions options) {

        this.logger = logger;
        this.game = game;
        this.menuMain = menuMain;
        this.options = options;

        camera = new OrthographicCamera();
        logger.debug("controller options menuMain camera", camera.toString());
        camera.setToOrtho(false, 800, 480);
        game.batch.setProjectionMatrix(camera.combined);

        Gdx.input.setInputProcessor(this);

    }


    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.BACK) {
            game.setScreen(options);
            Gdx.input.setInputProcessor(options);
        }

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        game.batch.begin();

        game.controllerTypeButton.setPosition(285, 110);
        game.controllerTypeButton.setSize(240, 50);
        game.controllerTypeButton.setAlpha(1f);

        game.controllerOptionsButton.setPosition(285, 210);
        game.controllerOptionsButton.setSize(240, 50);
        game.controllerOptionsButton.setAlpha(1f);

        if (Gdx.input.justTouched()) {
            Vector3 touchCoordinates = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            Vector3 screenCoordinates = camera.unproject(touchCoordinates);

            if (game.controllerOptionsButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)
                    && game.controllerType != ControllerType.TOUCH
                    && game.controllerType != ControllerType.PICTURE_IN_PICTURE)
            {
                game.controllerOptionsButton.setAlpha(0.5f);
                game.setScreen(new MenuControllerOptions(logger, game, menuMain, this)); // Starts game
                dispose();
            }
            else if (game.controllerTypeButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                game.controllerTypeButton.setAlpha(0.5f);
                game.setScreen(new MenuControllerType(logger, game, menuMain, this)); // Starts game
                dispose();
            }

        }

        game.controllerTypeButton.draw(game.batch);
        game.controllerOptionsButton.draw(game.batch);

        game.font.getData().setScale(4f);
        game.font.setColor(Color.WHITE);
        game.font.draw(game.batch, "Controllers", 265, 350);
        game.font.getData().setScale(2f);
        game.font.draw(game.batch, "Controller Type", 305, 150);
        if (game.controllerType == ControllerType.TOUCH || game.controllerType == ControllerType.PICTURE_IN_PICTURE)
            game.font.setColor(Color.LIGHT_GRAY);
        game.font.draw(game.batch, "Controller Options", 290, 250);
        if (game.controllerType == ControllerType.TOUCH || game.controllerType == ControllerType.PICTURE_IN_PICTURE)
            game.font.setColor(Color.WHITE);
        game.batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
