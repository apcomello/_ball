package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.graphics.Camera;

public class GameController {
    protected GameScreen screen;
    Logger logger;
    MenuMain menuMain;
    GameEntry game;

    public GameController(GameScreen screen, Logger logger, MenuMain menuMain, GameEntry game) {
        this.screen = screen;
        this.logger = logger;
        this.menuMain = menuMain;
        this.game = game;
    }

    public void movePaddle(float axisX, float axisY, Paddle p1Paddle) {
    }

    public void movePaddle(int screenX, int screenY, Paddle p1Paddle) {
    }

    public void movePaddle(int screenX, int screenY, Paddle p1Paddle, Camera cam) {
    }

    public void poll() {
    }
}
