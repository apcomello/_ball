package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.DefaultShaderProvider;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.CollisionObjectWrapper;
import com.badlogic.gdx.physics.bullet.collision.btCollisionAlgorithm;
import com.badlogic.gdx.physics.bullet.collision.btCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.collision.btCollisionDispatcher;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btDefaultCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.collision.btDispatcher;
import com.badlogic.gdx.physics.bullet.collision.btDispatcherInfo;
import com.badlogic.gdx.physics.bullet.collision.btManifoldResult;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class GameMain implements Screen, GameScreen {
    private Logger logger;

    private PerspectiveCamera cam;
    private ModelBatch gameModelBatch;
    private ModelBatch controllerModelBatch;
    private Array<Entity> playField;
    private Environment environment;
    private btCollisionConfiguration collisionConfig;
    private btDispatcher dispatcher;

    private Ball ball;
    private Wall wall;
    private Wall frontWall;
    private Wall backWall;
    private Paddle p1Paddle;
    private Paddle p2Paddle;

    private MenuMain menuMain;
    private GameEntry game;
    private GameController controller;
    private GameState gameState;

    private int height;
    private int width;
    private int camFOV;
    private Viewport gameViewport;
    private Viewport controllerViewport;
    private ShapeRenderer shapeRenderer;

    private float inGameTime;
    private int score;
    private boolean loadFromFile;

    GameMain(Logger logger, GameEntry game, MenuMain menuMain, boolean loadFromFile) {
        Bullet.init();

        this.logger = logger;
        this.game = game;
        this.menuMain = menuMain;
        this.loadFromFile = loadFromFile;

        inGameTime = 0f;
        score = 0;

        shapeRenderer = new ShapeRenderer();

        gameState = GameState.INITIAL_STATE;

        width = Gdx.graphics.getWidth();
        height = Gdx.graphics.getHeight();

        // Controller
        if (game.preferences.contains("controllerType") && this.loadFromFile)
            game.controllerType = ControllerType.values()[game.preferences.getInteger("controllerType")];

        controller = ControllerType.getController(game.controllerType, this, logger, menuMain, game);

        // Lighting
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

        // Camera
        camFOV = (game.controllerType == ControllerType.PICTURE_IN_PICTURE) ? 90 : 67;
        cam = new PerspectiveCamera(camFOV, width, height);
        cam.position.set(0f, 0f, 10f);
        cam.lookAt(0, 0, 0);
        cam.near = 1f;
        cam.far = 300f;
        if (game.controllerType == ControllerType.PICTURE_IN_PICTURE)
            cam.rotate(90, 0, 0, 1);
        cam.update();

        // Viewports
        if (game.controllerType == ControllerType.PICTURE_IN_PICTURE) {
            gameViewport = new FitViewport(width, height * 2, cam);
            gameViewport.setScreenBounds(0, 0, width / 2, height);

            controllerViewport = new FitViewport(width, height * 2, cam);
            controllerViewport.setScreenBounds(width / 2, 0, width / 2, height);
        }
        else {
            gameViewport = new FitViewport(width, height, cam);
            gameViewport.setScreenBounds(0, 0, width, height);
        }
        gameViewport.apply();

        // Entities
        playField = new Array<Entity>();
        gameModelBatch = new ModelBatch();
        controllerModelBatch = new ModelBatch(new DefaultShaderProvider() {
            @Override
            protected Shader createShader (Renderable renderable) {
                return new ShaderGrayscale(renderable);
            }
        });

        // Ball
        ball = new Ball(logger)
                .setRadius(0.5f)
                .setPosition(0f, 0f, 5.5001f)
                .setColor(Color.YELLOW);
        playField.add(ball);

        // P1 paddle
        p1Paddle = new Paddle(logger)
                .setId("p1 paddle")
                .setWidth(1.5f)
                .setHeight(1f)
                .setCenter(0f, 0f, 6f)
                .setLowerLeft(-0.75f, -0.5f, 0f)
                .setLowerRight(0.75f, -0.5f, 0f)
                .setUpperRight(0.75f, 0.5f, 0f)
                .setUpperLeft(-0.75f, 0.5f, 0f)
                .setNormal(0f, 0f, -1f)
                .setColor(Color.WHITE)
                .setAlphaBlend(0.75f)
                .setBounceSound(Gdx.audio.newSound(Gdx.files.internal("p1PaddleBounce.mp3")));
        playField.add(p1Paddle);

        // P2 paddle
        p2Paddle = new Paddle(logger)
                .setId("p2 paddle")
                .setWidth(1.5f)
                .setHeight(1f)
                .setMaxSpeed(2.5f)
                .setCenter(0f, 0f, -6f)
                .setLowerLeft(-0.75f, -0.5f, 0f)
                .setLowerRight(0.75f, -0.5f, 0f)
                .setUpperRight(0.75f, 0.5f, 0f)
                .setUpperLeft(-0.75f, 0.5f, 0f)
                .setNormal(0f, 0f, 1f)
                .setTexture(new Texture("p2_paddle.png"))
                .setColor(Color.WHITE)
                .setAlphaBlend(0.75f)
                .setBounceSound(Gdx.audio.newSound(Gdx.files.internal("p2PaddleBounce.mp3")));
        playField.add(p2Paddle);

        // Back wall
        backWall = new Wall(logger)
                .setId("back wall")
                .setWidth(8f)
                .setHeight(5f)
                .setCenter(0f, 0f, -6.5f)
                .setLowerLeft(-4f, -2.5f, 0f)
                .setLowerRight(4f, -2.5f, 0f)
                .setUpperRight(4f, 2.5f, 0f)
                .setUpperLeft(-4f, 2.5f, 0f)
                .setNormal(0f, 0f, 1f)
                .setColor(Color.CLEAR)
                .setBounceSound(Gdx.audio.newSound(Gdx.files.internal("miss.mp3")));
        playField.add(backWall);

        // Front wall
        frontWall = new Wall(logger)
                .setId("front wall")
                .setWidth(8f)
                .setHeight(5f)
                .setCenter(0f, 0f, 6.5f)
                .setLowerLeft(-4f, -2.5f, 0f)
                .setLowerRight(4f, -2.5f, 0f)
                .setUpperRight(4f, 2.5f, 0f)
                .setUpperLeft(-4f, 2.5f, 0f)
                .setNormal(0f, 0f, -1f)
                .setColor(Color.CLEAR)
                .setBounceSound(Gdx.audio.newSound(Gdx.files.internal("miss.mp3")));
        playField.add(frontWall);

        // Left wall
        wall = new Wall(logger)
                .setId("left wall")
                .setWidth(8f)
                .setHeight(5f)
                .setCenter(-4f, 0f, 0f)
                .setLowerLeft(0f, -2.5f, 6f)
                .setLowerRight(0f, -2.5f, -6f)
                .setUpperRight(0f, 2.5f, -6f)
                .setUpperLeft(0f, 2.5f, 6f)
                .setNormal(1f, 0f, 0f)
                .setColor(Color.GREEN)
                .setBounceSound(Gdx.audio.newSound(Gdx.files.internal("wallBounce.mp3")));
        playField.add(wall);

        // Right wall
        wall = new Wall(logger)
                .setId("right wall")
                .setWidth(8f)
                .setHeight(5f)
                .setCenter(4f, 0f, 0f)
                .setLowerLeft(0f, 2.5f, 6f)
                .setLowerRight(0f, 2.5f, -6f)
                .setUpperRight(0f, -2.5f, -6f)
                .setUpperLeft(0f, -2.5f, 6f)
                .setNormal(-1f, 0f, 0f)
                .setColor(Color.GREEN)
                .setBounceSound(Gdx.audio.newSound(Gdx.files.internal("wallBounce.mp3")));
        playField.add(wall);

        // Ceiling
        wall = new Wall(logger)
                .setId("ceiling")
                .setWidth(8f)
                .setHeight(5f)
                .setCenter(0f, 2.5f, 0f)
                .setLowerLeft(-4f, 0f, 6f)
                .setLowerRight(-4f, 0f, -6f)
                .setUpperRight(4f, 0f, -6f)
                .setUpperLeft(4f, 0f, 6f)
                .setNormal(0f, -1f, 0f)
                .setColor(Color.GREEN)
                .setBounceSound(Gdx.audio.newSound(Gdx.files.internal("wallBounce.mp3")));
        playField.add(wall);

        // Floor
        wall = new Wall(logger)
                .setId("floor")
                .setWidth(8f)
                .setHeight(5f)
                .setCenter(0f, -2.5f, 0f)
                .setLowerLeft(4f, 0f, 6f)
                .setLowerRight(4f, 0f, -6f)
                .setUpperRight(-4f, 0f, -6f)
                .setUpperLeft(-4f, 0f, 6f)
                .setNormal(0f, 1f, 0f)
                .setColor(Color.GREEN)
                .setBounceSound(Gdx.audio.newSound(Gdx.files.internal("wallBounce.mp3")));
        playField.add(wall);

        collisionConfig = new btDefaultCollisionConfiguration();
        dispatcher = new btCollisionDispatcher(collisionConfig);
    }

    @Override
    public void render(float delta) {
        // calculate our physical time step since the last frame
        final float timeDelta = Math.min(1f / 30f, Gdx.graphics.getDeltaTime());
        inGameTime += timeDelta;

        // handle motion controller input polling
        controller.poll();

        if (gameState == GameState.PLAYING_STATE) {
            // move the ball by a multiple of its velocity in relation to the time step
            ball.displaceByFactorAmount(timeDelta);

            // move the enemy paddle
            if (!ball.getVelocity().isZero()) {
                Vector3 destination = new Vector3();

                if (ball.getVelocity().z < 0) { // try and meet the ball
                    destination.x = ball.getPosition().x;
                    destination.y = ball.getPosition().y;
                } else if (ball.getVelocity().z > 0) { // go back to the center
                    destination.x = backWall.getCenter().x;
                    destination.y = backWall.getCenter().y;
                }
                destination.z = p2Paddle.getCenter().z;

                p2Paddle.moveTo(destination, timeDelta, true);
            }

            // check and handle the ball's collisions
            for (Entity e : playField) {
                if ((e instanceof Paddle || e instanceof Wall)
                        && checkCollision(ball.getCollisionObject(), e.getCollisionObject())) {
                    Wall w = (Wall) e;

                    // play sound effect
                    w.getBounceSound().play();

                    // if it's the back or front wall, stop the ball
                    if (w.getId() == "back wall" || w.getId() == "front wall") {
                        ball.setVelocity(0f, 0f, 0f);

                        // if it's the front or back wall, the game is over, calculate the score
                        if (w.getId() == "back wall") {
                            gameState = GameState.ENDED_WIN_STATE;
                            score = 10 * ((int)Math.min(Math.max(10000 - inGameTime * 100, 0), 10000) + 10000);
                        }
                        else {
                            gameState = GameState.ENDED_LOSS_STATE;
                            score = 10 * (int)Math.min(Math.max(inGameTime * 100, 0), 10000);
                        }

                        break; // exit the loop
                    }

                    // flip ball's direction using the wall's normal vector
                    Vector3 newBallVelocity = ball.getVelocity();
                    if (w.getNormal().x != 0)
                        newBallVelocity.x *= -1;
                    if (w.getNormal().y != 0)
                        newBallVelocity.y *= -1;
                    if (w.getNormal().z != 0)
                        newBallVelocity.z *= -1;
                    ball.setVelocity(newBallVelocity);

                    // if it's a paddle, add its instant velocity to the ball's velocity
                    if (w.getId() == "p1 paddle" || w.getId() == "p2 paddle") {
                        Paddle p = (Paddle) w;
                        Vector3 paddleVelocity = p.getInstantVelocity();
                        ball.setVelocity(ball.getVelocity().add(paddleVelocity.scl(10f)));
                    }

                    // displace inwards as to avoid getting stuck in collision
                    // TODO: calculate a displacementAmount instead of while-looping
                    while (checkCollision(ball.getCollisionObject(), e.getCollisionObject())) {
                        Vector3 zDisplacement = w.getNormal().scl(timeDelta);
                        ball.displace(zDisplacement);
                    }
                }
            }
        }

        // clear the scene
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        // render main screen
        gameModelBatch.begin(cam);

        for (Entity e : playField)
            gameModelBatch.render(e.getModelInstance(), environment);

        gameModelBatch.end();

        // render controller screen
        if (game.controllerType == ControllerType.PICTURE_IN_PICTURE) {
            controllerViewport.apply();

            controllerModelBatch.begin(cam);

            for (Entity e : playField)
                controllerModelBatch.render(e.getModelInstance(), environment);

            controllerModelBatch.end();

            gameViewport.apply();
        }

        if (gameState == GameState.ENDED_WIN_STATE || gameState == GameState.ENDED_LOSS_STATE) {
            // Needed for overlay
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

            // Draw transparent white rectangle over screen
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(new Color(1f, 1f, 1f, 0.5f));
            shapeRenderer.rect(0, 0, width, height);
            shapeRenderer.end();

            Gdx.gl.glDisable(GL20.GL_BLEND);

            // Write to screen
            game.batch.begin();

            if (game.controllerType == ControllerType.PICTURE_IN_PICTURE)
                game.batch.setTransformMatrix(new Matrix4().setToRotation(0, 0, 1, 90));

            game.font.getData().setScale(4f);

            // TODO: fixed coordinates don't work on multiple resolutions
            // TODO: this is basically 'position: absolute' from CSS
            if (gameState == GameState.ENDED_WIN_STATE) {
                game.font.setColor(Color.BLUE);

                if (game.controllerType == ControllerType.PICTURE_IN_PICTURE)
                    game.font.draw(game.batch, "YOU WIN!", 100, -300);
                else
                    game.font.draw(game.batch, "YOU WIN!", 260, 350);
            }
            else {
                game.font.setColor(Color.RED);

                if (game.controllerType == ControllerType.PICTURE_IN_PICTURE)
                    game.font.draw(game.batch, "YOU LOSE", 80, -300);
                else
                    game.font.draw(game.batch, "YOU LOSE", 245, 350);
            }

            game.font.getData().setScale(3f);
            game.font.setColor(Color.BLACK);

            if (game.controllerType == ControllerType.PICTURE_IN_PICTURE)
                game.font.draw(game.batch, "SCORE: " + String.valueOf(score), 75, -400);
            else
                game.font.draw(game.batch, "SCORE: " + String.valueOf(score), 230, 250);

            if (game.controllerType == ControllerType.PICTURE_IN_PICTURE)
                game.batch.setTransformMatrix(new Matrix4().setToRotation(0, 0, 1, 0));

            game.batch.end();
        }
    }

    private Boolean checkCollision(btCollisionObject obj0, btCollisionObject obj1) {
        CollisionObjectWrapper co0 = new CollisionObjectWrapper(obj0);
        CollisionObjectWrapper co1 = new CollisionObjectWrapper(obj1);

        btCollisionAlgorithm algorithm = dispatcher.findAlgorithm(co0.wrapper, co1.wrapper);

        btDispatcherInfo info = new btDispatcherInfo();
        btManifoldResult result = new btManifoldResult(co0.wrapper, co1.wrapper);

        algorithm.processCollision(co0.wrapper, co1.wrapper, info, result);

        Boolean hasCollision = result.getPersistentManifold().getNumContacts() > 0;

        dispatcher.freeCollisionAlgorithm(algorithm.getCPointer());
        result.dispose();
        info.dispose();
        co1.dispose();
        co0.dispose();

        return hasCollision;
    }

    @Override
    public void dispose() {
        // TODO: figure out where to call this when game "ends", because it is not automatically called
        collisionConfig.dispose();
        dispatcher.dispose();
        gameModelBatch.dispose();
        controllerModelBatch.dispose();

        for (Entity e : playField)
            e.dispose();
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void show() {
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void pointerHover(int screenX, int screenY) {
        controller.movePaddle(screenX, screenY, p1Paddle, cam);
    }

    @Override
    public void pointerDown(int screenX, int screenY) {
        controller.movePaddle(screenX, screenY, p1Paddle, cam);
    }

    @Override
    public void pointerDrag(int screenX, int screenY) {
        controller.movePaddle(screenX, screenY, p1Paddle, cam);
    }

    @Override
    public void pointerUp(int screenX, int screenY) {
        controller.movePaddle(screenX, screenY, p1Paddle, cam);

        if (gameState == GameState.INITIAL_STATE
                && checkCollision(ball.getCollisionObject(), p1Paddle.getCollisionObject()))
        {
            p1Paddle.getBounceSound().play();

            Vector3 ballInitialVelocity = new Vector3();
            ballInitialVelocity.x = p1Paddle.getInstantVelocity().x * 10f;
            ballInitialVelocity.y = p1Paddle.getInstantVelocity().y * 10f;
            ballInitialVelocity.z = 7f;
            ball.setVelocity(ballInitialVelocity);

            gameState = GameState.PLAYING_STATE;
        }
    }

    @Override
    public void analog(float axisX, float axisY) {
        controller.movePaddle(axisX, axisY, p1Paddle);
    }

    @Override
    public void motion(float axisX, float axisY) {
        controller.movePaddle(axisX, axisY, p1Paddle);
    }

    @Override
    public void buttonPress(int buttonCode) {
        if (gameState == GameState.INITIAL_STATE
                && checkCollision(ball.getCollisionObject(), p1Paddle.getCollisionObject()))
        {
            p1Paddle.getBounceSound().play();

            Vector3 ballInitialVelocity = new Vector3();
            ballInitialVelocity.x = p1Paddle.getInstantVelocity().x * 10f;
            ballInitialVelocity.y = p1Paddle.getInstantVelocity().y * 10f;
            ballInitialVelocity.z = 7f;
            ball.setVelocity(ballInitialVelocity);

            gameState = GameState.PLAYING_STATE;
        }
    }
}