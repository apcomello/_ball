package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;

public class ControllerSplitScreen extends GameController implements InputProcessor {
    private Boolean touched;

    ControllerSplitScreen(GameScreen screen, Logger logger, MenuMain menuMain, GameEntry game) {
        super(screen, logger, menuMain, game);

        touched = false;

        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void movePaddle(int screenX, int screenY, Paddle p1Paddle, Camera cam) {
        Vector3 oldPosition = p1Paddle.getCenter();

        // force touch coordinates into bottom half of the screen
        screenX = Math.max(screenX, Gdx.graphics.getWidth() / 2);
        screenX = (screenX - Gdx.graphics.getWidth() / 2) * 2;

        // calculate new position by moving only along x and y
        Vector3 screenCoords = new Vector3(screenX, screenY, 0.75f);
        Vector3 newWorldCoords = cam.unproject(screenCoords);
        float newPosX = Math.min(Math.max(newWorldCoords.x, -4f + 0.75f), 4f - 0.75f); // don't go off-screen
        float newPosY = Math.min(Math.max(newWorldCoords.y, -2.5f + 0.5f), 2.5f - 0.5f); // don't go off-screen
        Vector3 newPosition = new Vector3(newPosX, newPosY, oldPosition.z);

        // move paddle
        Vector3 displacement = newPosition.cpy().sub(oldPosition);
        p1Paddle.displace(displacement);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.BACK)
            game.setScreen(menuMain);

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        touched = true;
        screen.pointerDown(screenX, screenY);

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (touched)
            screen.pointerUp(screenX, screenY);

        touched = false;

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (touched)
            screen.pointerDrag(screenX, screenY);

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        screen.pointerDrag(screenX, screenY);
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
