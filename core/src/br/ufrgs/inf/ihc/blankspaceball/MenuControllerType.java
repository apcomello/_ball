package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class MenuControllerType implements Screen, InputProcessor {

    private Logger logger;
    public GameEntry game;
    public MenuMain menuMain;
    public MenuController menuController;
    private OrthographicCamera camera;

    public MenuControllerType(Logger logger, GameEntry game, MenuMain menuMain, MenuController menuController) {

        this.logger = logger;
        this.game = game;
        this.menuMain = menuMain;
        this.menuController = menuController;

        camera = new OrthographicCamera();
        logger.debug("menuMain camera", camera.toString());
        camera.setToOrtho(false, 800, 480);
        game.batch.setProjectionMatrix(camera.combined);

        Gdx.input.setInputProcessor(this);

    }

    @Override
    public void show() {
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        game.batch.begin();

        game.touchControllerButton.setPosition(290, 325);
        game.touchControllerButton.setSize(235, 50);
        if (game.controllerType == ControllerType.TOUCH)
            game.touchControllerButton.setAlpha(1f);
        else
            game.touchControllerButton.setAlpha(0.7f);

        game.pipControllerButton.setPosition(290, 225);
        game.pipControllerButton.setSize(235, 50);
        if (game.controllerType == ControllerType.PICTURE_IN_PICTURE)
            game.pipControllerButton.setAlpha(1f);
        else
            game.pipControllerButton.setAlpha(0.7f);

        game.bluetoothControllerButton.setPosition(290, 125);
        game.bluetoothControllerButton.setSize(235, 50);
        if (game.controllerType == ControllerType.BLUETOOTH)
            game.bluetoothControllerButton.setAlpha(1f);
        else
            game.bluetoothControllerButton.setAlpha(0.7f);

        game.motionControllerButton.setPosition(290, 25);
        game.motionControllerButton.setSize(235, 50);
        if (game.controllerType == ControllerType.MOTION)
            game.motionControllerButton.setAlpha(1f);
        else
            game.motionControllerButton.setAlpha(0.7f);

        if (Gdx.input.justTouched()) {
            Vector3 touchCoordinates = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            Vector3 screenCoordinates = camera.unproject(touchCoordinates);

            if (game.touchControllerButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                game.controllerType = ControllerType.TOUCH;
                game.preferences.putInteger("controllerType", ControllerType.TOUCH.ordinal());
                game.touchControllerButton.setAlpha(0.5f);
                dispose();
            }
            else if (game.pipControllerButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                game.controllerType = ControllerType.PICTURE_IN_PICTURE;
                game.preferences.putInteger("controllerType", ControllerType.PICTURE_IN_PICTURE.ordinal());
                game.pipControllerButton.setAlpha(0.5f);
                dispose();
            }
            else if (game.bluetoothControllerButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                if (Controllers.getControllers().size > 0) { // no controller connected
                    game.controllerType = ControllerType.BLUETOOTH;
                    game.preferences.putInteger("controllerType", ControllerType.BLUETOOTH.ordinal());
                }
                else {
                    game.controllerType = ControllerType.TOUCH;
                    game.preferences.putInteger("controllerType", ControllerType.TOUCH.ordinal());
                }
                game.bluetoothControllerButton.setAlpha(0.5f);
                dispose();
            }
            else if (game.motionControllerButton.getBoundingRectangle().contains(screenCoordinates.x, screenCoordinates.y)) {
                game.controllerType = ControllerType.MOTION;
                game.preferences.putInteger("controllerType", ControllerType.MOTION.ordinal());
                game.motionControllerButton.setAlpha(0.5f);
                dispose();
            }
        }

        game.touchControllerButton.draw(game.batch);
        game.pipControllerButton.draw(game.batch);
        game.bluetoothControllerButton.draw(game.batch);
        game.motionControllerButton.draw(game.batch);

        game.font.getData().setScale(4f);
        game.font.setColor(Color.WHITE);
        game.font.draw(game.batch, "Controller Type ", 210, 455);
        game.font.getData().setScale(2f);
        game.font.draw(game.batch, "Touch", 365, 365);
        game.font.draw(game.batch, "Split Screen", 330, 265);
        game.font.draw(game.batch, "Bluetooth", 345, 165);
        game.font.draw(game.batch, "Motion", 360, 65);
        game.batch.end();

        game.preferences.flush();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.BACK) {
            game.setScreen(menuController);
            Gdx.input.setInputProcessor(menuController);
        }

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
