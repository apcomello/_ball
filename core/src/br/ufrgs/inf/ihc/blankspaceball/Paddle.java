package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.utils.Disposable;

public class Paddle extends Wall implements GdxModel, BtCollision, Disposable {
	// basic members
	private String id = "";
	private float width = 1f;
	private float height = 1f;
	private float maxSpeed = 2.5f;
    private Vector3 instantVelocity = new Vector3(0f, 0f, 0f);
	private Vector3 center = new Vector3(0f, 0f, 0f);
	private Vector3 lowerLeft = new Vector3(-1f, -1f, 0f);
	private Vector3 lowerRight = new Vector3(1f, -1f, 0f);
	private Vector3 upperLeft = new Vector3(-1f, 1f, 0f);
	private Vector3 upperRight = new Vector3(1f, 1f, 0f);
	private Vector3 normal = new Vector3(0f, 0f, 1f);
	private Color color = Color.BLACK;
	private float alphaBlend = 1f;
	private Sound bounceSound;

	// model members
	private Model model;
	private ModelBuilder modelBuilder;
	private ModelInstance modelInstance;
	private Texture texture;

	// collision members
	private btCollisionShape collisionShape;
	private btCollisionObject collisionObject;

	// logging
	private Logger logger;

	Paddle(Logger logger) {
		super(logger);

		this.logger = logger;
	}

	// region Getters
	public  String getId() {
		return this.id;
	}

	public float getWidth() {
		return this.width;
	}

	public float getHeight() {
		return this.height;
	}

	public float getMaxSpeed() {
		return this.maxSpeed;
	}

    public Vector3 getInstantVelocity() {
        return this.instantVelocity.cpy();
    }

    public Vector3 getCenter() {
		return this.center.cpy();
	}

	public Vector3 getLowerLeft() {
		return this.lowerLeft.cpy();
	}

	public Vector3 getLowerRight() {
		return this.lowerRight.cpy();
	}

	public Vector3 getUpperLeft() {
		return this.upperLeft.cpy();
	}

	public Vector3 getUpperRight() {
		return this.upperRight.cpy();
	}

	public Vector3 getNormal() {
		return this.normal.cpy();
	}

	public Color getColor() {
		return this.color.cpy();
	}

	public float getAlphaBlend() {
		return this.alphaBlend;
	}

	public Texture getTexture() {
		return this.texture;
	}

	public Sound getBounceSound() {
		return this.bounceSound;
	}
	// endregion

	// region Setters
	public Paddle setId(String id) {
		this.id = id;
		return this;
	}

	public Paddle setWidth(float width) {
		this.width = width;

		if (this.modelInstance != null)
			recreateModelInstance();

		return this;
	}

	public Paddle setHeight(float height) {
		this.height = height;

		if (this.modelInstance != null)
			recreateModelInstance();

		return this;
	}

	public Paddle setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;

		return this;
	}

	public Paddle setCenter(Vector3 center) {
		this.center = center.cpy();

		if (this.modelInstance != null)
			recreateModelInstance();

		return this;
	}

	public Paddle setCenter(float x, float y, float z) {
		return this.setCenter(new Vector3(x, y, z));
	}

	public Paddle setLowerLeft(Vector3 lowerLeft) {
		this.lowerLeft = lowerLeft.cpy();

		if (this.modelInstance != null)
			recreateModelInstance();

		return this;
	}

	public Paddle setLowerLeft(float x, float y, float z) {
		return this.setLowerLeft(new Vector3(x, y, z));
	}

	public Paddle setLowerRight(Vector3 lowerRight) {
		this.lowerRight = lowerRight.cpy();

		if (this.modelInstance != null)
			recreateModelInstance();

		return this;
	}

	public Paddle setLowerRight(float x, float y, float z) {
		return this.setLowerRight(new Vector3(x, y, z));
	}

	public Paddle setUpperLeft(Vector3 upperLeft) {
		this.upperLeft = upperLeft.cpy();

		if (this.modelInstance != null)
			recreateModelInstance();

		return this;
	}

	public Paddle setUpperLeft(float x, float y, float z) {
		return this.setUpperLeft(new Vector3(x, y, z));
	}

	public Paddle setUpperRight(Vector3 upperRight) {
		this.upperRight = upperRight.cpy();

		if (this.modelInstance != null)
			recreateModelInstance();

		return this;
	}

	public Paddle setUpperRight(float x, float y, float z) {
		return this.setUpperRight(new Vector3(x, y, z));
	}

	public Paddle setNormal(Vector3 normal) {
		this.normal = normal.cpy().nor();

		if (this.modelInstance != null)
			recreateModelInstance();

		return this;
	}

	public Paddle setNormal(float x, float y, float z) {
		return this.setNormal(new Vector3(x, y, z));
	}

	public Paddle setColor(Color color) {
		this.color = color.cpy();

        if (this.color.equals(Color.CLEAR))
            this.alphaBlend = 0f;

        if (this.modelInstance != null)
            recreateModelInstance();

		return this;
	}

	public Paddle setAlphaBlend(float alphaBlend) {
		this.alphaBlend = alphaBlend;

        if (this.modelInstance != null)
            recreateModelInstance();

		return this;
	}

	public Paddle setTexture(Texture texture) {
		this.texture = texture;

        if (this.modelInstance != null)
            recreateModelInstance();

		return this;
	}

	public Paddle setBounceSound(Sound bounceSound) {
		if (this.bounceSound != null)
			this.bounceSound.dispose();

		this.bounceSound = bounceSound;

		return this;
	}
	// endregion

	public ModelInstance getModelInstance() {
		if (modelInstance != null)
			return modelInstance;

		if (modelBuilder == null)
			modelBuilder = new ModelBuilder();

        if (color.equals(Color.CLEAR))
            alphaBlend = 0f;

		if (texture == null)
			texture = new Texture("p1_paddle.png");

        model = modelBuilder.createRect(
                this.lowerLeft.x, this.lowerLeft.y, this.lowerLeft.z,
                this.lowerRight.x, this.lowerRight.y, this.lowerRight.z,
                this.upperRight.x, this.upperRight.y, this.upperRight.z,
                this.upperLeft.x, this.upperLeft.y, this.upperLeft.z,
                this.normal.x, this.normal.y, this.normal.z,
                new Material(
                        TextureAttribute.createDiffuse(texture),
                        ColorAttribute.createDiffuse(this.color),
                        new BlendingAttribute(this.alphaBlend)
                ),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.TextureCoordinates
        );
        modelInstance = new ModelInstance(model);
        modelInstance.transform.translate(this.center);

		return modelInstance;
	}

	public void recreateModelInstance() {
		this.modelInstance = null;
		getModelInstance();

		if (collisionObject != null)
			recreateCollisionObject();
	}

	public btCollisionObject getCollisionObject() {
		if (collisionObject != null)
			return collisionObject;

		if (modelInstance == null)
			modelInstance = getModelInstance();

		BoundingBox AABB = new BoundingBox();
		modelInstance.calculateBoundingBox(AABB);

		Vector3 boxShape = new Vector3();
		AABB.getDimensions(boxShape);

		collisionShape = new btBoxShape(boxShape.cpy().scl(1f / 2f));
		collisionObject = new btCollisionObject();
		collisionObject.setCollisionShape(collisionShape);
		collisionObject.setWorldTransform(modelInstance.transform);

		return collisionObject;
	}

	public void recreateCollisionObject() {
		collisionObject = null;
		getCollisionObject();
	}

	public void displace(Vector3 displacement) {
		this.center.add(displacement);
		this.instantVelocity = displacement.cpy();

		if (modelInstance != null) {
			modelInstance.transform.translate(displacement);

			if (collisionObject != null)
				collisionObject.setWorldTransform(modelInstance.transform);
		}
	}

	public void moveTo(Vector3 destination, float deltaTime, Boolean limitSpeed) {
        Vector3 displacement = destination.cpy().sub(center);

        displacement.limit(maxSpeed * deltaTime);

        displace(displacement);
    }

	@Override
	public void dispose() {
		model.dispose();
		collisionShape.dispose();
		collisionObject.dispose();
	}
}
