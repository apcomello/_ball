package br.ufrgs.inf.ihc.blankspaceball;

import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;

public interface BtCollision {
    btCollisionObject getCollisionObject();
}
