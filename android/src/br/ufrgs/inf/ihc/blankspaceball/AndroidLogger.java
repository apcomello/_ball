package br.ufrgs.inf.ihc.blankspaceball;

import android.util.Log;

public class AndroidLogger implements Logger {
    public void debug(String tag, String message) {
        Log.d(tag, message);
    }

    public void error(String tag, String message) {
        Log.e(tag, message);
    }

    public void info(String tag, String message) {
        Log.i(tag, message);
    }

    public void warn(String tag, String message) {
        Log.w(tag, message);
    }
}
