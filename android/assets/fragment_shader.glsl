#ifdef GL_ES
    precision mediump float;
#endif

uniform vec4 u_color;
uniform int u_has_texture;
uniform sampler2D u_texture;
uniform float u_alpha;

varying vec2 v_texCoord0;

void main() {
    vec4 c = u_color;

    if (u_has_texture == 1)
         c = c * texture2D(u_texture, v_texCoord0);

    float gray = dot(c.rgb, vec3(0.299, 0.587, 0.114));
    gl_FragColor = vec4(gray, gray, gray, u_alpha);
}